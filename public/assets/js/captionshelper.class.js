CaptionsHelper = function() {
	this.itemData=null;
	// Esp. for the 't' parameter
	this.captionOffset=0; // In seconds
	this.captionStart=5; // In seconds
	this.captionDuration=7; // In seconds
	this.detailsCaptionsGap=5; // In seconds
	this.songDetails=null; // To hold details JSON data
	this.captionsData==new Array; // Contains all the captions in the song

	// Set actual objects of caption data for the song
	// based on data that has been read.
	this.setCaptionsForSong = function() {
			// Clear captions from previous song.
			this.captionsData=[];
			// If there is no country data for an item, it is probably
			// not a song and special video or something. So don't show
			// caption.
			if (this.itemData[COL_COUNTRY]!='') {
				var ratingText=getShortRatingOutput(this.itemData[COL_RATING]);
				this.addNewCaption({
					id: this.getACaptionId(),
					captionText: (this.itemData[COL_COUNTRY] + '... ' + ratingText),
					captionClass: 'country',
					start: this.captionOffset + this.captionStart,
					duration: this.captionDuration,
					visible: false
				});
			}
			// If we have song details data, show it.
			if (this.songDetails) {
				// TODO: Implement localization to remove this hardcoding
				var currentLang='bn';
				if (this.songDetails[currentLang].review) {
					this.addNewCaption({
						id: this.getACaptionId(),
						captionText: this.songDetails[currentLang].review,
						captionClass: 'details',
						start: this.captionOffset+(this.captionStart+this.captionDuration+this.detailsCaptionsGap),
						duration: this.captionDuration,
						visible: false
					});
				}
				if (this.songDetails[currentLang].facts.length > 0) {
					for (var i = 0; i < this.songDetails[currentLang].facts.length; i++) {
						this.addNewCaption({
							id: this.getACaptionId(),
							captionText: this.songDetails[currentLang].facts[i],
							captionClass: 'details',
							start: this.captionOffset+(30+((i+1)*(this.captionDuration+this.detailsCaptionsGap))),
							duration: this.captionDuration,
							visible: false
						});
					}
				}
			}
	}

	// Sets the data for the whole song.
	// This will assume a new song is being played when called.
	this.setItemData = function(itemData) {
		this.itemData=itemData;
		// Clear details data from previous song
		this.songDetails=null;
		var filename=getDetailsFilename(this.itemData[COL_VIDEO_URL]);
		// Set the value for 't' variable
		// TODO: Add check for youtube
		this.captionOffset=parseInt(getParam('t',this.itemData[COL_VIDEO_URL])) || 0;
		console.log('captionOffset:'+this.captionOffset);
		fetch(filename, {cache: "no-cache"})
			.then(response => response.text())
			.then(text => {
					console.log(text);
					// When file not found, it may return "Not Found".
					// This ignores that output and checks for a JSON
					// initiating "{" so that non-JSON data is not passed
					// to JSON parser.
					if (text!='Not Found' && text.charAt(0) == '{') {
						this.songDetails=JSON.parse(text);
					}
					this.setCaptionsForSong();
					console.log(this.captionsData);
				})
			// In some host setups (e.g. GitLab Pages) it may not return
			// "Not Found", rather trigger an error.
			.catch((error) => {
					this.setCaptionsForSong();
					console.error('Details Caption Fetch Error:', error);
				});
	}

	// Returns a random id for a caption
	this.getACaptionId = function() {
		return parseInt(Math.random()*999999);
	}

	// Create a new caption (and hide it after a duration)
	this.addNewCaption = function(captionData) {
		// TODO: Find a better way to keep this value in hand
		// A bad idea for dynamicity, but ok if value needs not to change
		captionData.end = captionData.start + captionData.duration;
		this.captionsData[this.captionsData.length]=captionData;
	}

	// Should be run every second the item plays
	// Data should have:
	// - current
	// - duration
	// - remaining
	// Works fine even with seeking
	this.secondTick = function(eventData) {
		for (var i = 0; i < this.captionsData.length; i++) {
			// TODO: Send effect directions when seeking (e.g. cut effect or not)
			// We've got to show this caption
			if ( this.captionsData[i].visible == false && (eventData.current >= this.captionsData[i].start && eventData.current <= this.captionsData[i].end) ) {
				this.captionsData[i].visible = true;
				$(document).trigger('captionsHShowCaption',this.captionsData[i]);
			// We've got to hide this caption
			} else if ( this.captionsData[i].visible == true && (eventData.current < this.captionsData[i].start || eventData.current > this.captionsData[i].end) ) {
				this.captionsData[i].visible = false;
				$(document).trigger('captionsHHideCaption',{
					id: this.captionsData[i].id
				});
			}
		}
	}
}