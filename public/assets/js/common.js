// ------ Configs ------ //

// Caption show/hide effect duration
EFFECT_TIME=500

// Column indexes in CSV (zero-based)
COL_COUNTRY=0;
COL_VIDEO_TITLE=1;
COL_VIDEO_URL=2;
COL_LISTING_DATE=3;
COL_RATING=4;
COL_SUBPLAYLIST=5;

// The separator used to separate data in a cell
FILTER_SEPARATOR=',';
RANGE_SEPARATOR='-';

// -- --
AjobCommon = function() {
}

// Returns short rating text
function getShortRatingOutput(ratingInt) {
	return parseInt(ratingInt)>6 ? '♥' : '☺️';
}

// Returns the video ID from video URL
var getVideoId = function(url) {
	// source: https://stackoverflow.com/a/8260383
	// TODO: Add other video providers as well, such as Vimeo.
	var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
	var match = url.match(regExp);
	return (match&&match[7].length==11)? match[7] : false;
}

// Returns where the details is stored for a specific video
var getDetailsFilename = function(videoUrl) {
	var videoId=getVideoId(videoUrl);
	var filename='./data/details/'+videoId.charAt(0).toLowerCase()+'/'+videoId+'.json';
	return filename;
}

// Gets GET request parameter
// Returns a URL parameter from either current URL or string URL
// Source: https://stackoverflow.com/a/831060
var getParam = function(name,url=null){
	if (url==null)
		url=location.search;
	if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(url))
		return decodeURIComponent(name[1]);
}