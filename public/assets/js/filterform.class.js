FilterForm = function() {
	if (!jQuery) {
		throw "JQuery is not declared";
	}

	// Return the form HTML for filtering
	this.renderForm = function(playlistData) {
		var html = '<form class="filterform">';
		var tags = this.getUniqueTags(playlistData);
		var countries = this.getUniqueCountries(playlistData);

		html += '<p>ট্যাগ: <div class="checks-container">'+this.renderCheckboxes(tags, 'tags')+'</div></p>';
		html += '<p><label>দেশ: '+this.renderDropdown(countries, 'countries', 'সব দেশ')+'</label></p>';
		html += '<p><label>রেটিং: </label>';
		html += '<label><input type="radio" name="rating" class="rating" value="" checked="checked"/> (সব রেটিং)</label>';
		html += '<label><input type="radio" name="rating" class="rating" value="0-6"/> ☺</label>';
		html += '<label><input type="radio" name="rating" class="rating" value="7-10"/> ♥</label>';
		html += '</p>';
		html += '<input type="hidden" name="params"/>';
		html += '<p class="buttonline">';
		html += '<a class="button submit" href="javascript:void(0);"><span class="icon play"></span>শুনুন</a> &nbsp;';
		html += '<a class="button submit" href="javascript:void(0);" data-param="order=reverse"><span class="icon reverse"></span></a> &nbsp;';
		html += '<a class="button submit" href="javascript:void(0);" data-param="order=shuffle"><span class="icon shuffle"></span></a>';
		html += '</p>';
		html += '</form>';

		return html;
	}
	// Get unique countries from playlist data
	this.getUniqueCountries = function(playlistData) {
		var uniqueCountries = [...new Set(playlistData.map((theitem) => theitem[COL_COUNTRY]))].sort();
		return uniqueCountries;
	}
	// Get unique tags from playlist data
	this.getUniqueTags = function(playlistData) {
		var tagsArray=[];
		for (i=0; i<playlistData.length; i++) {
			tagsArray = tagsArray.concat( playlistData[i][COL_SUBPLAYLIST].split(FILTER_SEPARATOR) )
		}
		var uniqueTags = [...new Set(tagsArray.map((theitem) => theitem))].sort();
		return uniqueTags;
	}
	// Convert string to something that can be used in HTML attributes
	this.getAttributeSafe = function(str) {
		return String(str).toLowerCase().replace(/ /g, '_').replace(/&/g, '_').replace(/</g, '').replace(/>/g, '').replace(/"/g, '');
	}
	// Return HTML select dropdown from array
	this.renderDropdown = function(dataArray, htmlName='', noneOption='') {
		var html = '<select class="'+htmlName+'" id="'+htmlName+'" name="'+htmlName+'">';
		if (noneOption!='') {
			html += '<option value="">('+noneOption+')</option>';
		}
		for (i=0; i<dataArray.length; i++) {
			html += '<option value="'+dataArray[i]+'">'+dataArray[i]+'</option>';
		}
		html += '</select>';
		return html;
	}
	this.renderCheckboxes = function(dataArray, htmlName='') {
		var html = '';
		for (i=0; i<dataArray.length; i++) {
			html += '<label><input type="checkbox" name="'+htmlName+'" value="'+this.getAttributeSafe(dataArray[i])+'" class="'+htmlName+'"> '+dataArray[i]+'</label>';
		}
		return html;
	}
	prepareParams = function(appendParam) {
		var params = [];
		// Tags
		var tags = [];
		$("input:checkbox[name='tags']:checked").each(function(){
			tags.push($(this).val());
		});
		if (tags.length > 0) {
			params.push( 'tags=' + tags.join(",") );
		}
		// Country
		if ( $("select[name=countries]").val() != '' ) {
			params.push( 'country=' + $("select[name=countries]").val() );
		}
		// Rating
		if ( $("input[name=rating]:checked").val() != '' ) {
			params.push( 'rating=' + $("input[name=rating]:checked").val() );
		}
		// Appended param (to handle the custom submit buttons)
		if (appendParam) {
			params.push( appendParam );
		}

		return params.join("&");
	}
	$('body').on('click', 'form.filterform .button.submit', function(e) {
		$('input[name=params]').val( prepareParams( $(this).attr('data-param') ) );
		$('form.filterform').submit();
	});
	$('body').on('submit', 'form.filterform', function(e) {
		e.preventDefault();
		$(document).trigger('filterFormSubmit',{
			params: $('input[name=params]').val()
		});
	});
}