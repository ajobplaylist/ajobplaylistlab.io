![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

# Ajob Desher Playlist (আজব দেশের প্লেলিস্ট)

If the title confuses you a little bit, it's normal. The words are from Bengali/Bangla language. "Ajob" means strange, weird, uncommon. "Desher" means relating to some country. So "Ajob Desher Playlist" means **"Playlist of Uncommon Countries"**.

It plays songs from different countries to you and optionally filters out the songs depending on the mood you are interested in. It has CSV files for playlists which are dynamically loaded and processed by JS to present the playlist on screen. Everything is static so you can enjoy it within browser or on a very simple server setup.


## Features

- Can generate the whole playlist automatically from a single csv file
- Categorized playlist supported
- Custom playlists depending on tags, categories and even countries
- Customized, modern, open source player Plyr.io used
- Completely static<sup>1</sup> - just HTML/CSS/JS
- Shows custom captions for country and other info<sup>2</sup> ()
- Code is open source<sup>3</sup>
- Open source JS projects used without CDNs (self contained and good for privacy)
- Privacy friendly<sup>4</sup>
- Ad free
- No cookies from this project as far tested<sup>5</sup>
- No proprietary drivers/software needed on system to work<sup>6</sup>

----

<sup>1</sup>But requires a server for fetch to work, any localhost would do. \
<sup>2</sup>Shown when available and when player controls are visible (except fullscreen) to keep screen clutter free. \
<sup>3</sup>But may not be strictly libre, since it streams from YouTube. \
<sup>4</sup>GitLab client is open source and GitLab.com has sensible [privacy policy](https://about.gitlab.com/privacy). So it should be ok for most users to use it like it is. If you are still serious about privacy or concerned of the requests made to YouTube, use private/incognito window and using [TOR](https://www.torproject.org) or a reliable VPN or at least a trustworthy proxy is recommended for privacy. Also, you can run locally to avoid GitLab.com servers (see below for instructions) with privacy solutions from above. \
<sup>5</sup>But YouTube sets cookies, so take privacy precautions above. Also, YouTube and this project uses Local Storage to store some settings and such. \
<sup>6</sup>Needs to be used in a modern browser. Recommened libre browsers are: IceCat/Iceweasel, Iridium, Brave.


## Features that may arrive in future

- Localization
- More video/audio platform support other than YT
- Live playlist filtering from player.html
- See song's detailed information from player.html
- Search feature for searching song/artist


## How do I listen to this playlist?

- Go to [ajobplaylist.gitlab.io](https://ajobplaylist.gitlab.io)
- then hit any play (►) button.
- Another way would be to run offline (see below).


## Troubleshooting

### Next song doesn't play when I'm on other tabs

**On IceCat/IceWeasel:**

1. Open a new tab and visit `about:config`
2. Press enter to continue
3. Search for `media.block-autoplay-until-in-foreground`
4. Set the value to `false` (double click to toggle value)
5. If you still have problems, right click anywhere on the page except the player -> select "View Page Info" -> Permissions -> Autoplay -> Uncheck "Use Default" -> Check "Allow Audio and Video"


**On Iridium or equivalent browsers:**

Unfortunately you can't allow to autoplay next song on Iridium and equivalent browsers when on other tabs, because the upstream project has deprecated this feature. Please use IceCat/IceWeasel with above steps to enjoy this feature.


### Song is not playing at all

**On IceCat/IceWeasel:**

(Use the steps from previous heading.)


**On Iridium or equivalent browsers:**

1. Visit [ajobplaylist.gitlab.io](https://ajobplaylist.gitlab.io)
2. Click the icon beside the website URL, click Site settings
3. Choose **Allow** on the dropdown combo beside **Autoplay**
4. Refresh the page


## Where is the data stored?

- Playlist CSV data is in `public/data` folder.
- Song details JSON data is in `public/data/details` folder.


## Details or documentation?

There are URL GET parameters and many different things in `DEVELOPING.md`.


## How to run locally?

If you want to run offline on a localhost environment, you can do so. This is especially useful for privacy-minded people or developers.

__Method 1 (if you already have a localhost setup)__

Just download or pull this repository and put on your webroot somewhere and use. It is just static HTML/CSS/JS. So it should work on any local server setup (even on vanilla Apache/nginx etc.)

__Method 2 (if you don't have a localhost setup)__

If you don't have a server setup and setting up and configuring a server seems hard, this is for you. Install [Node.js](https://nodejs.org/en/download/)+[NPM](https://nodejs.org/en/download/) and install [simple-server](https://www.npmjs.com/package/simple-server):

_* If you are using Windows, you will need to ommit `sudo` from these commands._

```
sudo npm install --global simple-server
```

When you want to use it, just `cd` to the repo dir and run:

```
sudo simple-server public 3300
```

Here, `public` is your webroot, where the servable files are and `3300` is the port number. Now you can open `http://localhost:3300` on your browser and enjoy!


# How to add a song

You have 2 options:

1. Run `node addsong` or on *nix `./addsong`. This is automated and easier to use. Node.js is required to be installed on the system. This will ask you to input data about the song in a step by step manner. At the end you should have your song added into the system. This can add both the song data to CSV and song details JSON file without needing any special spreadsheet or text editing software having to be installed.

2. Manually. You can use any spreadsheet application that supports CSV (such as LibreOffice Calc, Gnumeric) to add or edit CSV files in `public/data` folder. You can use any text editor (such as Notepad++, Gedit, Mousepad etc.) to save or edit json files inside `public/data/details`.


# License

This project uses other open source projects. So licenses will apply from their respective projects. The songs are not created by me and I do not own their copyright. The rights belong to their respective copyright holder(s). They are streamed from their source websites, so policies may apply from them. If you feel anything infringes copyright or you do not wish to see your song listed here, raise a [public issue](https://gitlab.com/ajobplaylist/ajobplaylist.gitlab.io/issues) or contact at: ajobplaylist<span>@</span>protonmail<span>.</span>com.

The code specifically to this project is licensed under MIT (Expat) License. The code can be used for commercial or non-commercial purpose.
